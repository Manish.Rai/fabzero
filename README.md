# FabZero
## First Day of FabZero
On the very first day of fab zero, the two instructors, Mr. Fran and Mr. Sibu from Spain and India, repectively, introduced themselves. They gave and overview about the FabZero and asked us to think about a Project that we can accomplish. 

Click [here](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/summary.md) to read more about FabZero.

Learn more [about me](https://gitlab.com/Manish.Rai/fabzero/-/blob/main/AboutMe.md)

The following is the Project that i have selected to accomplish during the entire FabZero course.
## BMI calculator: 
A loadcell and ultrasonic sensors will be used as the input of the project and 1 LCD screen as the output of the project. The loadcell will be placed at the bottom where the person stands and the ultrasonic sensor will be place at the top to check the height of the person.

### Nine Principles to follow to become a good fabber.
![9 principles](https://gitlab.com/PemaThinleyDHI/fabzero/-/raw/main/images/principles.jpeg)
### The Moto
![the moto](https://gitlab.com/PemaThinleyDHI/fabzero/-/raw/main/images/learnmakeshare.jpg)

## Inkscape
### Differences between Raster image and Vector image

Raster image
![raster_lion](raster_lion.png)

Vector Image
![vector_lion](vector_lion.png)

## Some basic linux commands
This command lets you download and get the details about the youtube or any video from any sites
``` Linux
youtube-dl <URL>
youtube-dl -F <URL>
```
This command lets you resize the image in your directory

``` Linux
convert -resize bigimage.jpg 640*640 small image
```

## PCB Designing
**KiCAD** is an open-source software suite for creating electronic circuit schematics, printed circuit boards (PCBs), and associated part descriptions. KiCad supports an integrated design workflow in which a schematic and corresponding PCB are designed together, as well as standalone workflows for special uses. KiCad also includes several utilities to help with circuit and PCB design, including a PCB calculator for determining electrical properties of circuit structures, a Gerber viewer for inspecting manufacturing files, a 3D viewer for visualizing the finished PCB, and an integrated SPICE simulator for inspecting circuit behavior.

KiCad runs on all major operating systems and a wide range of computer hardware. It supports PCBs with up to 32 copper layers and is suitable for creating designs of all complexities. KiCad is developed by a volunteer team of software and electrical engineers around the world with a mission of creating free and open-source electronics design software suitable for professional designers. 

### Hotkey shortcuts for KiCAD

| **HotKey** | **Accomplishment** | **Implementation**|
| ---------- | ------------------ | -----------------:|
| **A**      |to add new component| Press **A**       |
| **M**      |to grab the component along with the connecting wire| hover on the component you want to move and press **M**|
| **G**      | to grab the component along with the connecting wire | hover on the component you want to grab with the wire and press **G**|
|**R**       | to rotate the component | hover on the component you want to rotate and press **R**|
|**W**       | to add wire | hover on where you want to connect the wire and press **W** |

### Schematic diagram demo

![schematic](Schematic_diagram.png)

### PCB Layout

![pcb](pcb_design.png)

### 3D view of final PCB

![3D_view](3D_view.png)



